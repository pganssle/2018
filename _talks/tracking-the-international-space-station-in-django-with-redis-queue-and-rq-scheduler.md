---
abstract: Have you ever looked up at the sky and wondered if the International Space
  Station was flying by at that moment? In this live coding adventure we will cover
  the basics of scheduling tasks with RQ in Django by building a web app that sends
  you text messages whenever the ISS passes by your location.
duration: 30
level: All
room: PennTop South
slot: 2018-10-05 13:00:00-04:00
speakers:
- Sam Agnew
title: Tracking the International Space Station in Django with Redis-Queue and RQ
  Scheduler
type: talk
video_url: https://youtu.be/lal6eWVCEJs
---

The international space station (ISS) is an orbital outpost circling high above our heads. Given a location on Earth (latitude, longitude) the ISS Open Notify API will compute the next `n` number of times that the ISS will be overhead. I will cover step by step how to build a web application that notifies users via text message when the ISS is passing over them. 

In this session, attendees will learn how to:
- Access and interpret data from the [ISS Open Notify API](http://open-notify.org/Open-Notify-API/ISS-Pass-Times/) on the exact timing on when the ISS will next pass a given geolocation.
- Use Redis Queue as a lightweight alternative to Celery for executing asynchronous tasks.
- Use RQ Scheduler to schedule tasks to be performed at a specific datetime.
- Integrate RQ and RQ Scheduler into a Django web application.
- Respond to webhook requests with Django.
- Access client-side geolocation data using JavaScript.
- Send text message notifications using the Twilio SMS API.

In the Django app built during this talk, Redis will be used to store subscriber information such as a user's phone number and geolocation. RQ and RQ Scheduler will be used to execute a function that sends a text message notification using the Twilio API at a given datetime. 

At the end of my talk, I will send a text message containing the code I wrote, and a walkthrough on how to get it running to all of the audience members who interacted with the live-coded web application from my session.