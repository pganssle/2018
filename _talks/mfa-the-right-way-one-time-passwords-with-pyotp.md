---
abstract: Multi-factor authentication is a must-have for modern secure applications.
  Thankfully, with PyOTP, enabling MFA is a breeze! We'll start by learning about
  the two algorithm options, HOTP and TOTP. Then we'll live-code adding MFA to an
  application, complete with QR codes for syncing with your phone.
duration: 30
level: Beginner
room: PennTop North
slot: 2018-10-05 10:45:00-04:00
speakers:
- Collin Stedman
title: 'MFA the Right Way: One-Time Passwords with PyOTP'
type: talk
video_url: https://youtu.be/rXH4nVoecBE
---

Multi-factor authentication is a must-have for modern secure applications. Thankfully, with PyOTP, enabling MFA is a breeze! We'll start by learning about the two flavors of one time passwords, HMAC-based and time-based. Then we'll live-code adding MFA to an application, complete with QR codes for syncing with your phone via Authy or Google Authenticator.