---
abstract: Mastermind is a logic-based guessing game. Many years ago, Donald Knuth
  described a way to win the game in 5 moves or less. We'll implement the game and
  the algorithm from the article. Come learn how to beat Mastermind and turn a paper
  by a famous scientist into code!
duration: 30
level: All
room: PennTop North
slot: 2018-10-06 10:45:00-04:00
speakers:
- Adam Forsyth
title: 'Beating Mastermind: Winning Games, Translating Math to Code, and Learning
  from Donald Knuth'
type: talk
video_url: https://youtu.be/2iCpnWYXPik
---

Come learn how to solve logic games with Python! Also, in the process, learn about minimax algorithms and how to turn papers into code. This is a skill some people learn in school, but I had to teach myself -- I don't have a degree in computer science. There are so many problems out there that are "solved" but don't have an easy-to-use, open source library. This talk with help you bridge that gap, both for fun and for work. I've always found playing with code to be the best way to learn, so the example I'll use here is a game called Mastermind that I played as a child. To make it even more interesting, the paper giving a solution to the game was written by Donald Knuth -- the "father of analysis of algorithms" and author of "The Art of Computer Science", one of the great reference books of CS. I'll start by describing the game and walking through the paper, then talk about how to turn the prose and mathematical notation into code. By the end, you'll feel more comfortable doing the same, understand minimax algorithms, and never lose at Mastermind again.

This talk will be accessible to programmers at all levels. The slides are mostly graphics and diagrams, with some simple Python code and excerpts from the [referenced paper](http://www.cs.uni.edu/~wallingf/teaching/cs3530/resources/knuth-mastermind.pdf).