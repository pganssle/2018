---
name: Max Schwartz
talks:
- Building a Read Aloud eBook in Python
---

Max Schwartz is a research engineer in the ETS NLP and Speech group. He has a Master's in Computational Linguistics and his work primarily focuses on text analysis.
