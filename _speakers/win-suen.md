---
name: Win Suen
talks:
- 'Large Scale Graph Mining with Spark: What I learned from mapping >15 million websites'
---

Hello! My name is Win. I’m a data scientist, an avid hiker, and a reader of books and papers. I love to hack and poke into steaming piles of data. I encounter and wrangle big data into submission every day. Many of our logs exceed terabytes of data daily, so having a responsive, pleasure-to-use, general purpose computing tool like Spark extends the life expectancy of the data scientist. Basically, I won’t shut up about Spark. Currently, I work at AppNexus (now part of AT&T Advertising and Analytics).

This talk relates to ongoing research on mining large graphs, a topic that fascinates me to no end. I will share some of the latest research leveraging graphs as a problem-solving framework, which will be of interest to even non-data scientists.