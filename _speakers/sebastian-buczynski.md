---
name: "Sebastian Buczy\u0144ski"
talks:
- Clean architecture in Python
---

I am a dedicated Pythonista, with a flair for finding weaknesses in software and whole processes. I gained my experience by working on a complex ICT platform for several years (recently as Technical Lead) and participating in a couple of startups. Currently, I co-operate with STX Next as Senior Python Developer. I organize Python meetups in Łódź, the centre of Poland.